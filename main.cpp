/*
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include <RosquilleraReforged/rf_engine.h>

#include "include/mainprocess.h"

#include <unistd.h>
#include <iostream>
#include <string>
#include <dirent.h>
using namespace std;

extern int execMode;
extern int population;
extern int reproduction;
extern int repopulation;
extern int autostop;
extern string dnaFile;
extern bool hide;
extern string folder;
extern bool load;

int main(int argc, char *argv[])
{
	//Establecemos el Working Directory
		string arg(argv[0]);
	  arg.erase(arg.find_last_of('/'), arg.size());
		chdir(arg.c_str());

	//Argumentos de entrada
		if(argc > 1)
		{
			if(strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0)
			{
				cout << "Modo de uso: ./dino [OPCIONES]" << endl << endl;
				cout << "Opciones:" << endl;
				cout << "  -h,  --help          muestra esta ayuda" << endl;
				cout << "  -l,  --learn         inicia el juego en modo aprendizaje" << endl;
				cout << "       --population    indica la población a evolucionar" << endl;
				cout << "       --reproduction  indica el porcentaje que podrá reproducirse" << endl;
				cout << "       --repopulation  indica el porcentaje de población que se repoblará" << endl;
				cout << "       --autostop      indica cada cuántas generaciones debe detenerse" << endl;
				cout << "       --load          extrae ADNs del directorio de carga para la generación 0" << endl;
				cout << "  -r,  --reproduce     ejecuta una IA con el aprendizaje señalado" << endl;
				cout << "  -c,  --contest       organiza una competición con los ADNs del directorio de carga" << endl;
				cout << "  -hi, --hide          esconde la ventana durante las simulaciones" << endl;
				cout << "  -f,  --folder        indica el directorio donde guardar/cargar los resultados" << endl;
				cout << endl << "Por ejemplo:" << endl;
				cout << "	./dino --learn --population 1000 --autostop 10 -hi" << endl << endl;
				cout << "Si tienes algún problema envía un email a <tuzmakel@gmail.com>" << endl << endl;
				exit(0);
			}

			for(int i = 1; i < argc; i++)
			{
				if(strcmp(argv[i], "--learn") == 0 || strcmp(argv[i], "-l") == 0)
				{
					execMode = 1;
				}
				else if(strcmp(argv[i], "--reproduce") == 0 || strcmp(argv[i], "-r") == 0)
				{
					execMode = 2;
					dnaFile = argv[++i];
				}
				else if(strcmp(argv[i], "--contest") == 0 || strcmp(argv[i], "-c") == 0)
				{
					execMode = 3;
				}
				else if(strcmp(argv[i], "--population") == 0)
				{
					population = atoi(argv[++i]);
				}
				else if(strcmp(argv[i], "--reproduction") == 0)
				{
					reproduction = atoi(argv[++i]);
				}
				else if(strcmp(argv[i], "--repopulation") == 0)
				{
					repopulation = atoi(argv[++i]);
				}
				else if(strcmp(argv[i], "--autostop") == 0)
				{
					autostop = atoi(argv[++i]);
				}
				else if(strcmp(argv[i], "--load") == 0)
				{
					load = true;
				}
				else if(strcmp(argv[i], "--hide") == 0 || strcmp(argv[i], "-hi") == 0)
				{
					hide = true;
				}
				else if(strcmp(argv[i], "--folder") == 0 || strcmp(argv[i], "-f") == 0)
				{
					folder = argv[++i];
				}
			}
		}

	//Si no existe el directorio de guardado de IAs damos error
		DIR *dir = opendir(folder.c_str());
		if(dir == nullptr)
		{
			cout << "El directorio asignado ('" << folder << "') no existe" << endl;
			return -1;
		}

	RF_Engine::Start<MainProcess>(false);
	return 0;
}
