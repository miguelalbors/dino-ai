/*
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef GAME_H
#define GAME_H

#include <RosquilleraReforged/rf_process.h>
#include "../include/parallax_layer.h"
#include "../include/agentmanager.h"
#include <queue>
using namespace std;

class Obstacle : public RF_Process
{
  public:
    Obstacle():RF_Process("Obstacle"){}
    virtual ~Obstacle(){}

    virtual void Update();

  private:
    bool poped = false;
};

class Cactus : public Obstacle
{
  public:
    Cactus():Obstacle(){}
    virtual ~Cactus(){}

    virtual void Start();
};

class Bird : public Obstacle
{
  public:
    Bird():Obstacle(){}
    virtual ~Bird(){}

    virtual void Start();
    virtual void Draw();
    float deltaFrame = 0.0;
    int frame = 0;
};

class Game : public RF_Process
{
    public:
      Game():RF_Process("Game"){}
      virtual ~Game(){}

      virtual void Start();
      virtual void Update();
      virtual void Draw();

      static Game *instance;
      float groundHeight;
      float gameSpeed = 500.0;

      int score = 0;
      float scoreDelta = 0.0;
      void updateScore();

      queue<RF_Process*> obstacles;

    private:
      string scoreText = "";
      RF_Process *scoreProcess;
      ParallaxLayer *grass;
      float delta = 0.0;
      float obstaclesDelta = 0.0;
};

#endif //GAME_H
