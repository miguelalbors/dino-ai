/*
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef AGENT_H
#define AGENT_H

#include "../include/dino.h"
#include "../include/brain.h"

struct Action
{
  bool run,jump,bend;

  Action():run(false),jump(false),bend(false){}
  Action(bool _run, bool _jump, bool _bend):run(_run), jump(_jump), bend(_bend){}
};

class Agent
{
  public:
    Agent(){}
    virtual ~Agent();

    Dino *dino;
    void Start();
    void Update();
    void Stop();
    virtual int Input();

    bool isAlive();
    float getScore();

    string name = "";

};

class PlayerAgent : public Agent
{
  public:
    PlayerAgent():Agent(){name = "Player";}
    virtual ~PlayerAgent(){}

    virtual int Input();
    bool pressed = false;
    int action;
};

class GeneticAgent : public Agent
{
  public:
    GeneticAgent():Agent(){}
    virtual ~GeneticAgent();

    virtual int Input();
    void Configure(DNA* _dna = nullptr, string _name = "");

    DNA *dna;
    Brain *brain;
};

#endif //AGENT_H
