/*
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef DINO_H
#define DINO_H

#include <RosquilleraReforged/rf_process.h>

enum DinoAction
{
  RUN = 0,
  JUMP = 1,
  BEND = 2
};

class Dino : public RF_Process
{
  public:
    Dino():RF_Process("Dino"){}
    virtual ~Dino(){}

    virtual void Start();
    virtual void Update();
    virtual void Draw();

    void setAction(int _action);
    bool alive = true;
    float score;

  private:
    int action = RUN;
    float deltaFrame = 0.0;
    int frame = 0;

    float jumpF(float x);
    float jump_stage = 0.0;
    bool jumping = false, bending = false;

};

#endif //DINO_H
