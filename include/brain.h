/*
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef BRAIN_H
#define BRAIN_H

#include "../include/brainlayer.h"
#include "../include/dna.h"
#include <vector>
using namespace std;

class Brain
{
  public:
    Brain(int n, ...);
    virtual ~Brain(){}

    void applyDNA(DNA *dna);
    void addConnection(int layer, int n, Connection *connection);
    void setBias(int layer, int n, float bias);
    int Update();

    vector<BrainLayer*> layers;

    void print();
};

#endif //BRAIN_H
