/*
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef AGENTMANAGER_H
#define AGENTMANAGER_H

#include "../include/agent.h"
#include "../include/dna.h"
#include <type_traits>
#include <vector>
#include <list>
using namespace std;

class AgentManager
{
  public:
    static AgentManager *instance;

    template<class T = AgentManager>
    static T* getInstance()
    {
      static_assert(std::is_base_of<AgentManager, T>::value, "T must derive from AgentManager");
      return reinterpret_cast<T*>(instance);
    }

    AgentManager();
    virtual ~AgentManager(){}

    template<class T>
    static T* addAgent()
    {
      static_assert(std::is_base_of<Agent, T>::value, "T must derive from Agent");
      T* tmp = new T();
      instance->agents.push_back(tmp);
      return tmp;
    }

    template<class T = Agent>
    static T* getAgent(int agent)
    {
      static_assert(std::is_base_of<Agent, T>::value, "T must derive from Agent");
      return reinterpret_cast<T*>(instance->agents[agent]);
    }

    static void Start();
    static void Update();
    static void Reset();
    virtual void _reset();

    static bool stillAlive();

    void ordenaAgentes();
    void insertaOrdenado(int agente);

    list<int> resultados;
    vector<Agent*> agents;
    bool hideWindow = false;
    bool autoStop = true;
};

class PersistentAgentManager : public AgentManager
{
  public:
    PersistentAgentManager():AgentManager(){}
    virtual ~PersistentAgentManager(){}

    virtual void _reset();
};

class GeneticAgentManager : public AgentManager
{
  public:
    GeneticAgentManager(int poblacion = 100, int reproduction = 10, int repoblation = 50, int autostop = 10):AgentManager()
    {
      population = poblacion;
      autogeneration = autostop;
      reproductionPercent = (reproduction >= 0 && reproduction <= 100) ? reproduction : 10;
      repoblationPercent = (repoblation >= 0 && repoblation <= 100) ? repoblation : 50;
    }

    virtual ~GeneticAgentManager(){}

    virtual void _reset();
    static void Initialize(bool from = false);

    //void insertaOrdenado(int a);
    vector<DNA*> best;
    int population;
    int reproductionPercent = 10;
    int repoblationPercent = 50;

    int generation = 0;
    int autogeneration = 1;
};

class ContestAgentManager : public PersistentAgentManager
{
  public:
    ContestAgentManager():PersistentAgentManager(){}
    virtual ~ContestAgentManager(){}

    static void Initialize();

    virtual void _reset();
};

#endif //AGENTMANAGER_H
