# Dino AI
Dino AI es un proyecto personal para aprender a programar una red neuronal basada en algoritmos genéticos.

## Dependencias
La lista de dependencias es la siguiente:
 * cmake >= 2.8.0
 * c++17
 * La rosquillera framework - Reforged (https://gitlab.com/yawin/RosquilleraReforged)

## Compilación
Estando en la carpeta del proyecto:

     cd ./bin
     cmake ..
     make all

## Ejecución
Modo de uso: `./dino [OPCIONES]`

    Opciones:
      -h,  --help          muestra la ayuda
      -l,  --learn         inicia el juego en modo aprendizaje
           --population    indica la población a evolucionar
           --reproduction  indica el porcentaje que podrá reproducirse
           --repopulation  indica el porcentaje de población que se repoblará
           --autostop      indica cada cuántas generaciones debe detenerse
           --load          extrae ADNs del directorio de carga para la generación 0
      -r,  --reproduce     ejecuta una IA con el aprendizaje señalado
      -c,  --contest       organiza una competición con los ADNs del directorio de carga
      -hi, --hide          esconde la ventana durante las simulaciones
      -f,  --folder        indica el directorio donde guardar/cargar los resultados

Por ejemplo:
> ./dino --learn --population 1000 --autostop 10 -hi

Si tienes algún problema envía un email a <tuzmakel@gmail.com>
