/*
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "../include/game.h"
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_textmanager.h>
#include <RosquilleraReforged/rf_input.h>
using namespace RF_Structs;

#include "../include/mainprocess.h"

Game* Game::instance = nullptr;

void Game::Start()
{
  instance = this;

  RF_AssetManager::loadAssetPackage("res/bird");
  RF_AssetManager::loadAssetPackage("res/cactus");
  RF_AssetManager::loadAssetPackage("res/dino");

  groundHeight = RF_Engine::MainWindow()->height()*0.75;

  grass = RF_Engine::getTask<ParallaxLayer>(RF_Engine::newTask<ParallaxLayer>(id));
  grass->graph = RF_AssetManager::Get<RF_Gfx2D>("other", "track");
  grass->transform.position = {0.0, groundHeight + 20.0};
  grass->zLayer = 1000;

  obstaclesDelta = 1.75;

  AgentManager::Start();
  window->setVisibility(!AgentManager::instance->hideWindow);
}

void Game::Update()
{
  delta += RF_Engine::instance->Clock.deltaTime;
  scoreDelta += RF_Engine::instance->Clock.deltaTime*gameSpeed/100;

  grass->Move({-RF_Engine::instance->Clock.deltaTime*gameSpeed,0.0});

  obstaclesDelta -= RF_Engine::instance->Clock.deltaTime;
  if(obstaclesDelta <= 0.0)
  {
    obstaclesDelta = 0.75 + ((float)(rand()%10))/10.0;
    if(obstaclesDelta > 3.0) obstaclesDelta = 3.0;

    if(rand()%100 < 10)
      obstacles.push(RF_Engine::getTask(RF_Engine::newTask<Bird>(id)));
    else
      obstacles.push(RF_Engine::getTask(RF_Engine::newTask<Cactus>(id)));
  }

  AgentManager::Update();
  gameSpeed += RF_Engine::instance->Clock.deltaTime*2;

  if(!AgentManager::stillAlive())
  {
    AgentManager::Reset();
    RF_TextManager::DeleteText(scoreText);
    signal = S_SLEEP;
    MainProcess::instance->loadState(1);
  }
}

void Game::updateScore()
{
  if(signal != S_AWAKE) return;

  if(scoreText != "") RF_TextManager::DeleteText(scoreText);
  scoreText = RF_TextManager::Write(to_string(score), {0,0,0}, {RF_Engine::MainWindow()->width(), 0});
  scoreProcess = RF_Engine::getTask(scoreText);
  scoreProcess->transform.position.x -= 10 + scoreProcess->graph->w*0.5;
  scoreProcess->transform.position.y += 10 + scoreProcess->graph->h*0.5;
}

void Game::Draw()
{
  score = (int)scoreDelta;
  updateScore();
}

void Cactus::Start()
{
  graph = RF_AssetManager::Get<RF_Gfx2D>("cactus", to_string(rand()%6));
  transform.position = {RF_Engine::MainWindow()->width() + graph->w*0.5, Game::instance->groundHeight - graph->h*0.5 + 25};
}

void Bird::Start()
{
  graph = RF_AssetManager::Get<RF_Gfx2D>("bird", "0");
  transform.position = {RF_Engine::MainWindow()->width() + graph->w*0.5, 0.0};
  switch(rand()%3)
  {
    case 0:
      transform.position.y = Game::instance->groundHeight - 25.0;
      break;
    case 1:
      transform.position.y = Game::instance->groundHeight - 50.0;
      break;
    case 2:
      transform.position.y = Game::instance->groundHeight - 125.0;
      break;
  }
}

void Bird::Draw()
{
  deltaFrame += RF_Engine::instance->Clock.deltaTime*Game::instance->gameSpeed/50;
  frame = (int)deltaFrame;
  graph = RF_AssetManager::Get<RF_Gfx2D>("bird", to_string(frame%2));
}

void Obstacle::Update()
{
  transform.position.x -= RF_Engine::instance->Clock.deltaTime*Game::instance->gameSpeed;
  if(transform.position.x < 100)
  {
    if(!poped)
    {
      poped = true;
      Game::instance->obstacles.pop();
    }

    if(transform.position.x < -100)
    {
      signal = S_KILL;
    }
  }
}
