/*
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "../include/dino.h"
#include "../include/game.h"
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_collision.h>

void Dino::Start()
{
  graph = RF_AssetManager::Get<RF_Gfx2D>("dino", "DinoStart");
  transform.position.x = 200;
  transform.position.y = Game::instance->groundHeight;
}

float Dino::jumpF(float x)
{
  return (-4 * x * (x-1)) * 200;
}

void Dino::Update()
{
  if(action == JUMP && !jumping)
  {
    jumping = true;
    action = RUN;
  }
  else if(action == BEND)
  {
    if(jumping)
    {
      jump_stage += (1.0 - jump_stage)/3.0;
    }
    else
    {
      bending = true;
      transform.position.y = Game::instance->groundHeight+17;
    }
  }
  else if(action == RUN)
  {
    bending = false;
    if(!jumping) transform.position.y = Game::instance->groundHeight;
  }

  if(jumping)
  {
    transform.position.y = Game::instance->groundHeight - jumpF(jump_stage);
    jump_stage += RF_Engine::instance->Clock.deltaTime;

    if(jump_stage > 1.0)
    {
      transform.position.y = Game::instance->groundHeight;
      jumping = false;
      jump_stage = 0.0;
    }
  }

  if(!Game::instance->obstacles.empty() && RF_Collision::checkCollision(this,Game::instance->obstacles.front(), RF_CT_PP))
  {
    alive = false;
    signal = S_SLEEP;
    score = Game::instance->scoreDelta;
  }
}

void Dino::Draw()
{
  deltaFrame += RF_Engine::instance->Clock.deltaTime*Game::instance->gameSpeed/50;
  frame = (int)deltaFrame;

  if(jumping)
  {
    graph = RF_AssetManager::Get<RF_Gfx2D>("dino", "DinoJump");
  }
  else if(bending)
  {
    graph = RF_AssetManager::Get<RF_Gfx2D>("dino", "DinoDuck"+to_string(1 + frame%2));
  }
  else
  {
    graph = RF_AssetManager::Get<RF_Gfx2D>("dino", "DinoRun"+to_string(1 + frame%2));
  }
}

void Dino::setAction(int _action)
{
  action = _action;
}
