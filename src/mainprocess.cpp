/*
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "../include/mainprocess.h"
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_soundmanager.h>
#include <RosquilleraReforged/rf_textmanager.h>

#include "../include/maintitle.h"
#include "../include/game.h"
#include "../include/brain.h"
#include "../include/agentmanager.h"
#include "../include/agent.h"

int execMode = 0; //0: Jugador, 1: Aprendizaje, 2: Reproducción
int population = 1000;
int reproduction = 10;
int repopulation = 50;
int autostop = 10;
string dnaFile = "";
bool load = false;
bool hide = false;

MainProcess* MainProcess::instance = nullptr;

void MainProcess::Start()
{
  instance = this;
  RF_Engine::addWindow("Dino AI", 1280, 720, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);//, SDL_WINDOW_OPENGL|SDL_WINDOW_FULLSCREEN);

  RF_AssetManager::loadAssetPackage("res/other");

  switch(execMode)
  {
    case 1:
      new GeneticAgentManager(population, reproduction, repopulation, autostop);
      GeneticAgentManager::Initialize(load);
      break;

    case 2:
    {
      DNA *dna = new DNA();
      dna->parseFromFile(dnaFile);

      new PersistentAgentManager();
      PersistentAgentManager::addAgent<GeneticAgent>()->Configure(dna);
      break;
    }

    case 3:
    {
      new ContestAgentManager();
      ContestAgentManager::Initialize();
      break;
    }

    default:
      new PersistentAgentManager();
      PersistentAgentManager::addAgent<PlayerAgent>();
      break;
  }

  AgentManager::instance->hideWindow = hide;
  loadState(1);
}

void MainProcess::loadState(int state)
{
  if(stateMachine != state)
  {
    if(scene != nullptr) RF_Engine::sendSignal(scene, RF_Structs::S_KILL);
    stateMachine = state;

    switch(stateMachine)
    {
      case 1:
        scene = RF_Engine::getTask(RF_Engine::newTask<Maintitle>(id));
        break;
      case 2:
        scene = RF_Engine::getTask(RF_Engine::newTask<Game>(id));
        break;
    }
  }
}

void MainProcess::Update()
{
  //ExeControl
    if(RF_Input::key[RF_Structs::_esc] || RF_Input::key[RF_Structs::_close_window])
    {
      RF_Engine::Status() = false;
    }
}
