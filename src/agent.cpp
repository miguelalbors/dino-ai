/*
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "../include/agent.h"
#include "../include/game.h"
#include <RosquilleraReforged/rf_engine.h>
using namespace RF_Structs;

Agent::~Agent()
{
  Stop();
}

void Agent::Start()
{
  dino = RF_Engine::getTask<Dino>(RF_Engine::newTask<Dino>());
}

void Agent::Stop()
{
  dino->signal = S_KILL;
}

void Agent::Update()
{
  dino->setAction(Input());
}

int Agent::Input()
{
  return RUN;
}

bool Agent::isAlive()
{
  return dino->alive;
}

float Agent::getScore()
{
  return dino->score;
}

int PlayerAgent::Input()
{
  if(!pressed && RF_Input::key[_up])
  {
    pressed = true;
    action = JUMP;
  }
  else if(!pressed && RF_Input::key[_down])
  {
    pressed = true;
    action = BEND;
  }
  else if(pressed && !RF_Input::key[_up] && !RF_Input::key[_down])
  {
    pressed = false;
    action = RUN;
  }

  return action;
}

GeneticAgent::~GeneticAgent()
{
    delete dna;
    delete brain;
}
void GeneticAgent::Configure(DNA* _dna, string _name)
{
  name = _name;
  brain = new Brain(3, 7,7,3);

  dna = _dna;
  if(dna == nullptr) dna = DNA::getRandDNA(3, 7,7,3);

  brain->applyDNA(dna);
}

int GeneticAgent::Input()
{
  if(Game::instance->obstacles.size() > 0)
  {
    brain->setBias(0, 0, Game::instance->obstacles.front()->transform.position.x - dino->transform.position.x);
    brain->setBias(0, 1, Game::instance->obstacles.front()->transform.position.x);
    brain->setBias(0, 2, Game::instance->obstacles.front()->transform.position.y);
    brain->setBias(0, 3, Game::instance->obstacles.front()->graph->w);
    brain->setBias(0, 4, Game::instance->obstacles.front()->graph->h);
  }
  else
  {
    brain->setBias(0, 0, -dino->transform.position.x);
    brain->setBias(0, 1, 0);
    brain->setBias(0, 2, 0);
    brain->setBias(0, 3, 0);
    brain->setBias(0, 4, 0);
  }

  brain->setBias(0, 5, dino->transform.position.y);
  brain->setBias(0, 6, Game::instance->gameSpeed);
  return brain->Update();
}
