/*
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "../include/parallax_layer.h"

#include <RosquilleraReforged/rf_engine.h>

void ParallaxLayer::Update()
{
  if(graph != nullptr)
  {
    GenerateLayer();
    return;
  }

  if(transform.position.x != 0.0 || transform.position.y != 0.0)
  {
    Move(transform.position);
    transform.position.x = transform.position.y = 0;
  }
}

void ParallaxLayer::Move(Vector2<float> movement)
{
  for(i = 0; i < tiles.size(); i++)
  {
    tiles[i]->transform.position.x += movement.x;
    tiles[i]->transform.position.y += movement.y;

    if(tiles[i]->transform.position.x < -(tiles[i]->graph->w>>1))
    {
      tiles[i]->transform.position.x += RF_Engine::MainWindow()->width()+tiles[i]->graph->w;
    }
  }
}

void ParallaxLayer::GenerateLayer()
{
  int n_tiles = ((float)(window->width()))/((float)(graph->w));
  if(window->width()%graph->w != 0) n_tiles++;
  n_tiles++;
  RF_Process *tile;

  for(i = 0; i < n_tiles; i++)
  {
    tile = RF_Engine::getTask(RF_Engine::newTask(id));
    tile->transform.position.x = transform.position.x + i*graph->w;
    tile->transform.position.y = transform.position.y;
    tile->graph = graph;
    tile->zLayer = zLayer;
    tiles.push_back(tile);
  }

  graph = nullptr;
  transform.position.x = transform.position.y = 0;
}

RF_Structs::Vector2<float> ParallaxLayer::getPosition()
{
  if(tiles.size() == 0) return {0.0,0.0};

  return tiles[0]->transform.position;
}

void ParallaxLayer::setHeight(float height)
{
  for(i = 0; i < tiles.size(); i++)
  {
    tiles[i]->transform.position.y = height;
  }
}
