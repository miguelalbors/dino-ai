/*
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "../include/maintitle.h"
#include "../include/mainprocess.h"
#include "../include/agentmanager.h"

#include <RosquilleraReforged/rf_primitive.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_textmanager.h>

using namespace RF_Structs;

void Maintitle::Start()
{
  RF_Engine::MainWindow()->setBackColor(0xE0,0xE0,0xE0);
  transform.position.x = RF_Engine::MainWindow()->width() >> 1;
  transform.position.y = RF_Engine::MainWindow()->height() >> 1;

  RF_TextManager::Font = RF_AssetManager::Get<RF_Font>("other", "Times_New_Roman");
  RF_TextManager::Write("Pulsa ENTER para empezar", {0x0F,0x0F,0x0F}, {RF_Engine::MainWindow()->width()>>1, 200 + RF_Engine::MainWindow()->height()>>1}, id);

  if(AgentManager::getInstance()->autoStop)
  {
    window->setVisibility(true);
  }
}

void Maintitle::Update()
{
  if(!AgentManager::getInstance()->autoStop)
  {
    RF_Engine::MainWindow()->setBackColor(0xE0,0xE0,0xE0);
    MainProcess::instance->loadState(2);
    signal = RF_Structs::S_KILL;
    return;
  }

  rand();
  if(!pressed && RF_Input::key[_return])
  {
    pressed = true;
  }
  else if((pressed && !RF_Input::key[_return]))
  {
    RF_Engine::MainWindow()->setBackColor(0xE0,0xE0,0xE0);
    MainProcess::instance->loadState(2);
    signal = RF_Structs::S_KILL;
  }
}
