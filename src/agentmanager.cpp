/*
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "../include/agentmanager.h"
#include <dirent.h>

string folder = "saves";

AgentManager* AgentManager::instance = nullptr;

AgentManager::AgentManager()
{
  if(instance != nullptr)
  {
    delete this;
    return;
  }

  instance = this;
}

void AgentManager::Start()
{
  for(Agent* agent : instance->agents)
  {
    agent->Start();
  }
}

void AgentManager::Update()
{
  for(Agent* agent : instance->agents)
  {
    agent->Update();
  }
}

void AgentManager::Reset()
{
  instance->_reset();
}
void AgentManager::_reset()
{
  for(Agent* agent : agents)
  {
    delete agent;
  }

  instance->agents.clear();
}

bool AgentManager::stillAlive()
{
  if(instance->agents.size() > 0)
  {
    for(Agent* agent : instance->agents)
    {
      if(agent->isAlive())
      return true;
    }
  }

  return false;
}

void AgentManager::ordenaAgentes()
{
  resultados.clear();

  for(int i = 0; i < agents.size(); i++)
  {
    insertaOrdenado(i);
  }
}

void AgentManager::insertaOrdenado(int agente)
{
  list<int> newList;

  bool insertado = false;

  for(int i : resultados)
  {
    if(!insertado && agents[agente]->getScore() > agents[i]->getScore())
    {
      newList.push_back(agente);
      insertado = true;
    }

    newList.push_back(i);
  }

  if(!insertado)
    newList.push_back(agente);

  resultados = list<int>(newList);
}

void PersistentAgentManager::_reset()
{
  for(Agent* agent : agents)
  {
    agent->Stop();
  }
}

void GeneticAgentManager::Initialize(bool load)
{
  int pob = AgentManager::getInstance<GeneticAgentManager>()->population;

  if(load)
  {
    DIR *dir = opendir(folder.c_str());
    struct dirent *ent;
    string name;

    DNA *dna;
    while((ent = readdir(dir)) != nullptr && instance->agents.size() < pob)
    {
      if((strcmp(ent->d_name, ".")!=0) && (strcmp(ent->d_name, "..")!=0) && (strcmp(ent->d_name, ".gitignore")!=0))
      {
        name = ent->d_name;
        const size_t pos = name.find_last_of('.');
        name.erase(pos);

        dna = new DNA();
        dna->parseFromFile(folder + "/" + name);
        GeneticAgentManager::addAgent<GeneticAgent>()->Configure(dna);
      }
    }

    if(instance->agents.size() < pob)
      cout << "Se van a crear " << pob - instance->agents.size() << " más para completar la población" << endl;
  }

  for(int i = instance->agents.size(); i < pob; i++)
  {
    GeneticAgentManager::addAgent<GeneticAgent>()->Configure();
  }
}
void GeneticAgentManager::_reset()
{
  //Limpiamos listas de mejores
    best.clear();

  //Ordenamos los agentes por resultado
    ordenaAgentes();

  cout << "La mejor puntuación de la generación " << generation << " ha sido " << agents[resultados.front()]->getScore() << endl;
  getAgent<GeneticAgent>(resultados.front())->dna->parseToFile(folder + "/" + to_string(generation));

  //Calculamos cuántos se van a reproducir
    int to_reproduce = reproductionPercent * population / 100;
    if(to_reproduce <= 0) to_reproduce = 1;

    int reproduction_share = (repoblationPercent * population / 100) / to_reproduce;

  //Obtenemos el ADN de los mejores
    for(int i = 0; i < to_reproduce; i++)
    {
      best.push_back(getAgent<GeneticAgent>(resultados.front())->dna->getCopy());
      resultados.pop_front();
    }

  //Eliminamos los agentes
    AgentManager::_reset();

  //Reproducimos los mejores
    for(int i = 0; i < best.size(); i++)
    {
      addAgent<GeneticAgent>()->Configure(best[i]);
      for(int j = 1; j < reproduction_share && agents.size() < population; j++)
      {
        addAgent<GeneticAgent>()->Configure(best[i]->getChild());
      }
    }

  //Rellenamos con nueva población hasta alcanzar la población deseada
    int cont = agents.size();
    while(agents.size() < population)
    {
      addAgent<GeneticAgent>()->Configure();
    }

  cout << "La nueva población es de " << agents.size() << " agentes, de los cuales " << best.size() << " son de generaciones anteriores, " << (reproduction_share*to_reproduce)-best.size() << " son descendientes y " << agents.size()-cont << " son nuevos" << endl;
  generation++;

  //Calculamos si la ejecución siguiente tiene que detenerse
  autoStop = (generation%autogeneration == 0);
}

void ContestAgentManager::Initialize()
{
  DIR *dir = opendir(folder.c_str());
  struct dirent *ent;
  string name;

  DNA *dna;
  while((ent = readdir(dir)) != nullptr)
  {
    if((strcmp(ent->d_name, ".")!=0) && (strcmp(ent->d_name, "..")!=0) && (strcmp(ent->d_name, ".gitignore")!=0))
    {
      name = ent->d_name;
      const size_t pos = name.find_last_of('.');
      name.erase(pos);

      dna = new DNA();
      dna->parseFromFile(folder + "/" + name);
      ContestAgentManager::addAgent<GeneticAgent>()->Configure(dna, name);
      cout << name << " listo para competir" << endl;
    }
  }

  cout << endl;
}

void ContestAgentManager::_reset()
{
  ordenaAgentes();

  cout << "Los resultados de la competición son:" << endl;

  //Escribimos la tabla de resultados
    for(int i = 0; !resultados.empty(); i++)
    {
      cout << "   " << ((i<9)?" ":"") << i+1 << "º  " << getAgent<GeneticAgent>(resultados.front())->name << "  (score: " << getAgent<GeneticAgent>(resultados.front())->getScore() << ")" << endl;
      resultados.pop_front();
    }

  //Paramos a los agentes
    PersistentAgentManager::_reset();
}
